import { Component, OnInit } from "@angular/core";
import { NavParams, ViewController, ToastController, AlertController, ModalController, IonicPage } from "ionic-angular";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ApiServiceProvider } from "../../../providers/api-service/api-service";
// import { GroupModalPage } from "./group-modal/group-modal";
import * as moment from 'moment';
import { AndroidPermissions } from "@ionic-native/android-permissions";
import { SMS } from "@ionic-native/sms";
import { BarcodeScanner } from "@ionic-native/barcode-scanner";

@IonicPage()
@Component({
    selector: 'page-add-devices',
    templateUrl: 'add-device-modal.html'
})

export class AddDeviceModalPage implements OnInit {
    addvehicleForm: FormGroup;
    submitAttempt: boolean;
    customer: any;
    devicesadd: any;
    islogin: any;
    allGroup: any = [];
    // allGroupName: any;
    deviceModel: any;
    selectUser: any;
    allVehicle: any;
    modeldata: any;
    groupstaus: any;
    userdata: any;
    vehType: any;
    groupstaus_id: any;
    devicedetails: any = {};
    TypeOf_Device: any;
    driverList: any;
    isloginn: any;
    minDate: any;
    currentYear: any;
    modeldata_id: any;
    rootLogin: any;
    isCustomer: boolean = false;
    constructor(
        public params: NavParams,
        public viewCtrl: ViewController,
        public formBuilder: FormBuilder,
        public apiCall: ApiServiceProvider,
        public toastCtrl: ToastController,
        public alerCtrl: AlertController,
        public modalCtrl: ModalController,
        public androidPermissions: AndroidPermissions,
        public sms: SMS,
        private barcodeScanner: BarcodeScanner
    ) {
        // console.log("very", this.params);
        // console.log("cust data", params.get("custDet"))
        var tempdate = new Date();
        tempdate.setDate(tempdate.getDate() + 365);

        this.currentYear = moment(new Date(tempdate), 'DD-MM-YYYY').format("YYYY-MM-DD");
        console.log(this.currentYear);

        // ============== one month later date from current date ================
        var tdate = new Date();
        var eightMonthsFromJan312009 = tdate.setMonth(tdate.getMonth() + 1);
        this.minDate = moment(new Date(eightMonthsFromJan312009), 'DD-MM-YYYY').format("YYYY-MM-DD");
        console.log("one one later date=> " + moment(new Date(eightMonthsFromJan312009), 'DD-MM-YYYY').format("YYYY-MM-DD"))
        // =============== end
        this.addvehicleForm = formBuilder.group({
            device_name: ['', Validators.required],
            device_id: ['', Validators.required],
            driver: [''],
            contact_num: [''],
            sim_number: ['', Validators.required],
            ExipreDate: [this.currentYear, Validators.required],
            device_type: ['', Validators.required],
            name: [''],
            first_name: [''],
            brand: ['', Validators.required],
            fastag: ['']
        });
        if (params.get("custDet")) {
            this.isCustomer = true;
            this.islogin = params.get("custDet");
            // this.islogin = JSON.parse(localStorage.getItem('details')) || {};

            console.log("custDel=> ", this.islogin);

            this.rootLogin = JSON.parse(localStorage.getItem('details')) || {};
            // this.rootLogin = params.get("custDet");
        } else {
            this.islogin = JSON.parse(localStorage.getItem('details')) || {};
            console.log("islogin devices => " + JSON.stringify(this.islogin));
        }
        console.log("cheack imp data", this.rootLogin);
    }
    dismiss() {
        this.viewCtrl.dismiss();
    }

    ngOnInit() {
        this.getGroup();
        this.getDeviceModel();
        this.getSelectUser();
        this.getVehicleType();
        this.getDrivers();
    }

    scan() {
        this.barcodeScanner.scan().then(barcodeData => {
            console.log('Barcode data', barcodeData);
            this.addvehicleForm.patchValue({
                device_id: barcodeData.text
            });
        }).catch(err => {
            console.log('Error', err);
        });
    }

    checkPermissionAndSend() {

        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.SEND_SMS).then(
            success => {
                if (!success.hasPermission) {
                    this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.SEND_SMS).
                        then((success) => {
                            // alert('hi')
                            this.sendMessage();
                        },
                            (err) => {
                                // alert("error1")
                                // alert(err);
                            });
                }
            },
            err => {
                this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.SEND_SMS).
                    then((success) => {
                        // alert('success')
                        this.sendMessage();
                    },
                        (err) => {
                            // alert('error2')
                            // alert(err);
                        });
            });
    }

    sendMessage() {
        // debugger
        if (this.sms) {
            //CONFIGURATION
            var options = {
                replaceLineBreaks: false, // true to replace \n by a new line, false by default
                android: {
                    intent: 'INTENT'  // send SMS with the native android SMS messaging
                    //intent: '' // send SMS without opening any other app
                }
            };
            this.sms.hasPermission()
                .then((perm) => {
                    if (perm) {
                        console.log("sms permission granted!!");
                        this.sms.send(this.addvehicleForm.value.sim_number, '', options);
                    } else {
                        this.sms.send(this.addvehicleForm.value.sim_number, '', options);

                        console.log("sms permission not granted!!")
                    }
                });
        }
    }

    getDrivers() {
        this.apiCall.getDriverList(this.islogin._id)
            .subscribe(data => {
                this.driverList = data;
                console.log("driver list => " + JSON.stringify(this.driverList))
            },
                err => {
                    console.log(err);
                });
    }


    openAddGroupModal() {
        let modal = this.modalCtrl.create('GroupModalPage');
        modal.onDidDismiss(() => {
            console.log("modal dismissed!")
        })
        modal.present();
    }
    // {
    //     "devicename": "hjvhj",
    //     "deviceid": "097987978686",
    //     "hrdwr": 987678667,
    //     "typdev": "Tracker",
    //     "emailid": "gaurav.gupta@adnate.in",
    //     "Mileage": "10",
    //     "sim_number": 987678667,
    //     "supAdmin": "59cbbdbe508f164aa2fef3d8",
    //     "device_model": "5baf3e857a0c5b647f2ed27a",
    //     "expdate": "2021-01-27T18:30:00.000Z",
    //     "user": "59cbbdbe508f164aa2fef3d8",
    //     "sim_provider": "jhvhgch",
    //     "vehicleType": "5b483afb72a00817af5a5079"
    //  }

    adddevices() {
        debugger;
        this.submitAttempt = true;
        if (this.addvehicleForm.valid) {
            console.log(this.addvehicleForm.value);
            if (this.params.get("custDet")) {
                if (this.rootLogin.isSuperAdmin == true) {
                    this.devicedetails = {
                        "devicename": this.addvehicleForm.value.device_name,
                        "deviceid": this.addvehicleForm.value.device_id,
                        "driver_name": this.addvehicleForm.value.driver,
                        "contact_number": this.addvehicleForm.value.contact_num,
                        "typdev": "Tracker",
                        "sim_number": this.addvehicleForm.value.sim_number,
                        "user": (this.userdata ? this.userdata._id : this.islogin._id),
                        "emailid": this.islogin.email,
                        // "iconType": this.TypeOf_Device,
                        // "vehicleGroup": this.groupstaus_id,
                        "device_model": this.modeldata_id,
                        "expdate": new Date(this.addvehicleForm.value.ExipreDate).toISOString(),
                        "supAdmin": this.rootLogin._id
                    }
                } else {
                    if (this.rootLogin.isDealer == true) {
                        this.devicedetails = {
                            "devicename": this.addvehicleForm.value.device_name,
                            "deviceid": this.addvehicleForm.value.device_id,
                            "driver_name": this.addvehicleForm.value.driver,
                            "contact_number": this.addvehicleForm.value.contact_num,
                            "typdev": "Tracker",
                            "sim_number": this.addvehicleForm.value.sim_number,
                            "user": (this.userdata ? this.userdata._id : this.islogin._id),
                            "emailid": this.islogin.email,
                            // "iconType": this.TypeOf_Device,
                            // "vehicleGroup": this.groupstaus_id,
                            "device_model": this.modeldata_id,
                            "expdate": new Date(this.addvehicleForm.value.ExipreDate).toISOString(),
                            "supAdmin": this.rootLogin.supAdmin,
                            "Dealer": this.rootLogin._id
                        }
                    }
                }

                if (this.addvehicleForm.value.fastag !== null || this.addvehicleForm.value.fastag !== undefined
                    || this.addvehicleForm.value.fastag !== "") {
                    this.devicedetails.fastag = this.addvehicleForm.value.fastag
                }

                if (this.groupstaus != undefined) {
                    this.groupstaus_id = this.groupstaus._id;
                    this.devicedetails.vehicleGroup = this.groupstaus_id
                }

                if (this.vehType == undefined) {
                    this.devicedetails;
                } else {
                    this.devicedetails.vehicleType = this.vehType._id;
                }
            } else {
                if (this.islogin.isSuperAdmin == true) {
                    this.devicedetails = {
                        "devicename": this.addvehicleForm.value.device_name,
                        "deviceid": this.addvehicleForm.value.device_id,
                        "driver_name": this.addvehicleForm.value.driver,
                        "contact_number": this.addvehicleForm.value.contact_num,
                        "typdev": "Tracker",
                        "sim_number": this.addvehicleForm.value.sim_number,
                        "user": (this.userdata ? this.userdata._id : this.islogin._id),
                        "emailid": this.islogin.email,
                        // "vehicleType": this.vehType,
                        // "vehicleGroup": this.groupstaus_id,
                        "device_model": this.modeldata_id,
                        "expdate": new Date(this.addvehicleForm.value.ExipreDate).toISOString(),
                        "supAdmin": this.islogin._id
                    }
                } else {
                    if (this.islogin.isDealer == true) {
                        this.devicedetails = {
                            "devicename": this.addvehicleForm.value.device_name,
                            "deviceid": this.addvehicleForm.value.device_id,
                            "driver_name": this.addvehicleForm.value.driver,
                            "contact_number": this.addvehicleForm.value.contact_num,
                            "typdev": "Tracker",
                            "sim_number": this.addvehicleForm.value.sim_number,
                            "user": (this.userdata ? this.userdata._id : this.islogin._id),
                            "emailid": this.islogin.email,
                            // "vehicleType": this.vehType,
                            // "vehicleGroup": this.groupstaus_id,
                            "device_model": this.modeldata_id,
                            "expdate": new Date(this.addvehicleForm.value.ExipreDate).toISOString(),
                            "supAdmin": this.islogin.supAdmin,
                            "Dealer": this.islogin._id
                        }
                    }
                }

                if (this.addvehicleForm.value.fastag != null || this.addvehicleForm.value.fastag != undefined
                    || this.addvehicleForm.value.fastag != "") {
                    this.devicedetails.fastag = this.addvehicleForm.value.fastag
                }

                if (this.groupstaus != undefined) {
                    this.groupstaus_id = this.groupstaus._id;
                    this.devicedetails.vehicleGroup = this.groupstaus_id
                }

                if (this.vehType == undefined) {
                    this.devicedetails;
                } else {
                    this.devicedetails.vehicleType = this.vehType._id;
                }
            }
            console.log("devicedetails=> " + this.devicedetails);

            this.apiCall.startLoading().present();
            this.apiCall.addDeviceCall(this.devicedetails)
                .subscribe(data => {
                    this.apiCall.stopLoading();
                    this.devicesadd = data;
                    console.log("devicesadd=> ", this.devicesadd)
                    let toast = this.toastCtrl.create({
                        message: 'Vehicle was added successfully',
                        position: 'top',
                        duration: 1500
                    });

                    toast.onDidDismiss(() => {
                        console.log('Dismissed toast');
                        this.viewCtrl.dismiss(this.vehType);
                    });

                    toast.present();
                },
                    err => {
                        this.apiCall.stopLoading();
                        var body = err._body;
                        var msg = JSON.parse(body);
                        console.log("error occured 1=> ", msg)
                        let alert = this.alerCtrl.create({
                            title: 'Oops!',
                            message: 'Device is already into system. Please delete from system and then try again.',
                            buttons: ['OK']
                        });
                        alert.present();
                    });
        }
    }

    getGroup() {
        console.log("get group");
        var baseURLp = this.apiCall.mainUrl + 'groups/getGroups_list?uid=' + this.islogin._id;
        this.apiCall.startLoading().present();
        this.apiCall.groupsCall(baseURLp)
            .subscribe(data => {
                this.apiCall.stopLoading();
                this.allGroup = data["group_details"];
            },
                err => {
                    console.log(err);
                    this.apiCall.stopLoading();
                });
    }

    getDeviceModel() {
        console.log("getdevices");
        var baseURLp = this.apiCall.mainUrl + 'deviceModel/getDeviceModel';
        this.apiCall.getDeviceModelCall(baseURLp)
            .subscribe(data => {
                this.deviceModel = data;
            },
                err => {
                    console.log(err);
                });
    }

    getSelectUser() {
        console.log("get user");
        var baseURLp = this.apiCall.mainUrl + 'users/getAllUsers?dealer=' + this.islogin._id
        this.apiCall.getAllUsersCall(baseURLp)
            .subscribe(data => {
                this.selectUser = data;
            },
                error => {
                    console.log(error)
                });
    }

    getVehicleType() {

        var baseURLp = this.apiCall.mainUrl + 'vehicleType/getVehicleTypes?user=' + this.islogin._id;
        this.apiCall.getVehicleTypesCall(baseURLp)
            .subscribe(data => {
                this.allVehicle = data;
            }, err => {
                console.log(err);
            });
    }

    deviceModelata(deviceModel) {
        debugger
        // if (deviceModel != undefined) {
            this.modeldata = this.addvehicleForm.value.device_type;
            this.modeldata_id = this.modeldata._id;
        // } else {
        //     this.modeldata_id = null;
        // }
    }

    GroupStatusdata(status) {
        this.groupstaus = status;
        console.log("groupstaus=> " + this.groupstaus._id);
    }

    userselectData(userselect) {
        this.userdata = userselect;
        //this.userdata = this.addvehicleForm.value.first_name;
        console.log("userdata=> " + this.userdata.first_name);
        console.log("user", this.userdata._id);
    }

    vehicleTypeselectData(vehicletype) {
        debugger
        // this.vehType = vehicletype;
        this.vehType = this.addvehicleForm.value.brand;
        console.log("vehType=> " + this.vehType._id);
    }
}

//problem is that
//abhi aap device add krege na to if you select user than it will be added but agar hum select nahi krte hai
// to error aata hai
//because custDet undefined aa jaa rha hai because humne kuch bheja hi nahi add-devics.ts
//add button ke click pr
